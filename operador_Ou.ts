// testando o operador_Ou 
namespace operador_Ou {
    let idade = 16; 
    let maiorIdade = idade > 18; 
    let possuiAutorizacaoDosPais = true; 
    
    let podeBeber = maiorIdade || possuiAutorizacaoDosPais; 

    console.log(podeBeber); // true
}
    